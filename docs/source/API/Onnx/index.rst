Aidge ONNX API
==============

.. autofunction:: aidge_onnx.load_onnx

Node converter
--------------

Register
~~~~~~~~

.. autofunction:: aidge_onnx.node_converter.register_converter

.. autofunction:: aidge_onnx.node_converter.supported_operators

.. autodecorator:: aidge_onnx.node_converter.auto_register

.. because defaultdict.__doc__ is readonly, Sphinx cannot use the docstring set in the code.
.. data:: aidge_onnx.node_converter.ONNX_NODE_CONVERTER_

    This ``defaultdict`` maps the ONNX type to a function which can convert an ONNX Node into an Aidge Node.
    This means that if a key is missing from :py:data:`aidge_onnx.node_converter.ONNX_NODE_CONVERTER_`, it will return the function :py:func:`aidge_onnx.node_converter.generic.import_generic` which import the ONNX node as an Aidge generic operator.
    It is possible to add keys to this dictionnary at runtime using :py:func:`aidge_onnx.node_converter.register_converter` or :py:func:`aidge_onnx.node_converter.auto_register`


Converters
~~~~~~~~~~

.. autofunction:: aidge_onnx.node_converter.generic.import_generic


