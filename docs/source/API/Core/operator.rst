Operators
=========

Operator
--------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.Operator
            :members:
            :inherited-members:

    .. tab-item:: C++

        .. doxygenclass:: Aidge::Operator


Add
---

.. jinja:: AddOp_in_out
   :file: jinja/op_in_out_mmd.jinja
   :header_char: -

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.Add

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::Add

Average Pooling
---------------

.. jinja:: AvgPoolingOp2D_in_out
   :file: jinja/op_in_out_mmd.jinja
   :header_char: -

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.AvgPooling1D

        .. autofunction:: aidge_core.AvgPooling2D

        .. autofunction:: aidge_core.AvgPooling3D

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::AvgPooling(DimSize_t const (&kernel_dims)[DIM],const std::string& name = "",const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t,DIM>(1),const std::array<DimSize_t, (DIM<<1)> &padding_dims = create_array<DimSize_t,(DIM<<1)>(0))


BatchNorm
---------

.. jinja:: BatchNormOp2D_in_out
   :file: jinja/op_in_out_mmd.jinja
   :header_char: -

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.BatchNorm2D

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::BatchNorm

Conv
----

.. jinja:: ConvOp2D_in_out
   :file: jinja/op_in_out_mmd.jinja
   :header_char: -


.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.Conv1D

        .. autofunction:: aidge_core.Conv2D

        .. autofunction:: aidge_core.Conv3D

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::Conv(DimSize_t in_channels, DimSize_t out_channels, DimSize_t const (&kernel_dims)[DIM], const std::string& name = "", const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t, DIM>(1), const std::array<DimSize_t, (DIM << 1)> &padding_dims = create_array<DimSize_t, (DIM << 1)>(0), const std::array<DimSize_t, DIM> &dilation_dims = create_array<DimSize_t, DIM>(1))


ConvDepthWise
-------------

.. jinja:: ConvDepthWiseOp2D_in_out
   :file: jinja/op_in_out_mmd.jinja
   :header_char: -

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.ConvDepthWise1D

        .. autofunction:: aidge_core.ConvDepthWise2D

        .. autofunction:: aidge_core.ConvDepthWise3D

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::ConvDepthWise(DimSize_t const (&kernel_dims)[DIM], const std::string& name = "", const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t, DIM>(1), const std::array<DimSize_t, (DIM << 1)> &padding_dims = create_array<DimSize_t, (DIM << 1)>(0), const std::array<DimSize_t, DIM> &dilation_dims = create_array<DimSize_t, DIM>(1))


FC
--

.. jinja:: FCOp_in_out
   :file: jinja/op_in_out_mmd.jinja
   :header_char: -

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.FC

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::FC

Generic Operator
----------------

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.GenericOperator

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::GenericOperator


LeakyReLU
---------

.. jinja:: LeakyReLUOp_in_out
   :file: jinja/op_in_out_mmd.jinja
   :header_char: -

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.LeakyReLU

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::LeakyReLU



Matmul
------

.. jinja:: MatMulOp_in_out
   :file: jinja/op_in_out_mmd.jinja
   :header_char: -


.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.MatMul

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::MatMul

Producer
--------

.. jinja:: ProducerOp_in_out
   :file: jinja/op_in_out_mmd.jinja
   :header_char: -


.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.Producer

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::Producer(DimSize_t const (&dims)[DIM], const std::string& name = "")


ReLU
----

.. jinja:: ReLUOp_in_out
   :file: jinja/op_in_out_mmd.jinja
   :header_char: -

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.ReLU

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::ReLU

Softmax
-------

.. jinja:: SoftmaxOp_in_out
   :file: jinja/op_in_out_mmd.jinja
   :header_char: -

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.Softmax

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::Softmax
