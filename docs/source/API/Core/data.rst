Data
====

Tensor
------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.Tensor
            :members:
            :inherited-members:

    .. tab-item:: C++
        
        .. doxygenclass:: Aidge::Tensor
