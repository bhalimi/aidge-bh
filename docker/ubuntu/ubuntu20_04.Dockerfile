# Aidge, Eclipse Public License 2.0
# Image is CPU-optimized for Aidge applications for training and inference

# Use the official Ubuntu 20.04 as base image
FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive

# Install linux packages
RUN apt update && apt install -y \
        build-essential \
        cmake \
        git \
        python3-dev \
        python3-pip \ 
        python3-virtualenv \
        tree

# Create installation directory
WORKDIR /usr/src/aidge

# Get Aidge from Gitlab Eclipse
RUN git clone --recursive https://gitlab.eclipse.org/eclipse/aidge/aidge.git /usr/src/aidge

# Install Aidge and required libraries 
RUN python3 -m pip install --upgrade pip wheel
RUN pip install --no-cache .

# Creates a symbolic link to make 'python' point to 'python3'
RUN ln -sf /usr/bin/python3 /usr/bin/python

WORKDIR /workspace
